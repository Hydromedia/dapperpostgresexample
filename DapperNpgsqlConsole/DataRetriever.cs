﻿using System;
using System.Collections.Generic;
using System.Data;

using Dapper;
using Npgsql;

namespace DapperNpgsqlConsole
{
    public class DataRetriever : IDisposable
    {
        private readonly IDbConnection dbConnection;

        public DataRetriever(string connectionString)
        {
            dbConnection = new NpgsqlConnection(connectionString);
            dbConnection.BeginTransaction(IsolationLevel.Serializable);
        }

        public IEnumerable<Dog> GetDogs()
        {
            return dbConnection.Query<Dog>("select * from dogs");
        }

        public IEnumerable<Style> GetStyles()
        {
            return dbConnection.Query<Style>("select * from styles");
        }

        public void UpdateDog(Dog d, string tableName)
        {
            dbConnection.Execute("update @table set val = @val where Id = @id", new {tableName, val, id = 1 });
        }

        public void InsertDog(Dog d, string tableName)
        {
            dbConnection.Execute("insert @table(val) values(@val)", new { tableName, val });
        }

        public void Dispose()
        {
            dbConnection.Close();
        }
    }
}
