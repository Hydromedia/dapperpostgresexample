﻿namespace DapperNpgsqlConsole
{
    public class Dog
    {
        public int Dog_Id { get; set; }
        public string Name { get; set; }
        public int Style_Id { get; set; }
    }
}
