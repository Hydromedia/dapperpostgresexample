﻿using System;
using System.Configuration;

namespace DapperNpgsqlConsole
{
    class Program
    {
        static void Main()
        {
            using (var t = new DataRetriever(ConfigurationManager.ConnectionStrings["dapper"].ToString()))
            {
                Console.WriteLine("Dogs{0}=====", Environment.NewLine);

                var dogs = t.GetDogs();

                foreach (var dog in dogs)
                {
                    Console.WriteLine("{0} - {1}", dog.Name, "test");
                }

                Console.WriteLine("{0}Styles{0}======", Environment.NewLine);

                var styles = t.GetStyles();

                foreach (var style in styles)
                {
                    Console.WriteLine("{0} - {1}", style.Name, style.Description);
                }
            }
            Console.ReadLine();
        }
    }
}
